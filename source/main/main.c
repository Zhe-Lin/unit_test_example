/**
 ******************************************************************************
 * File Name          : main.cpp
 * Description        : Main program body
 ******************************************************************************
 *
 *
 ******************************************************************************
 */
// Includes ------------------------------------------------------------------
// standard include
#include <stdio.h>

// user include
#include "logic/data_process.h"

// Internal variables ---------------------------------------------------------

// Extern variables -----------------------------------------------------------

// Function--------------------------------------------------------------------
/**
 * @brief   main.
 * @date    2018/07/30
 * @author  zhe-lin Zhang
 * @param   
 * @retval
*/
int main()
{
    printf("Hello World!\r\n");
    printf("decrease:%d\r\n", decrease());
    printf("Hello World!\r\n");
    return 0;
}
