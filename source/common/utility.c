#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utility.h"

void GetRandom(uint8_t *bRandom)
{
    int32_t i;
    //srand(time(NULL)); //ORG

    uint32_t count;
    static uint32_t count_2 = 0x00;

    count = (uint32_t)(&bRandom);
    count = (count & (0xA5 << count_2));
    if (24 > count_2)
    {
        count_2++;
    }
    else
    {
        count_2 = 0x00;
    }
    srand(count);

    for (i = 0; i < 8; i++)
    {
        bRandom[i] = (uint8_t)(rand() % 256);
    }
}

int32_t Value4ByteComp_le(uint8_t *ut1, uint8_t *ut2)
{
    int32_t ret = 0;
    int32_t i;
    for (i = 3; i >= 0; i--)
    {
        if (ut1[i] > ut2[i])
        {
            ret = 1;
            break;
        }
        else if (ut1[i] < ut2[i])
        {
            ret = -1;
            break;
        }
    }
    return ret;
}

int32_t Value4ByteToInt(uint8_t *byteValue)
{
    int32_t i;
    int32_t intValue = 0;
    for (i = 3; i >= 0; i--)
        intValue = (intValue << 8) + byteValue[i];
    return intValue;
}

uint16_t UnsignedValue2ByteToIntLe(uint8_t *byteValue)
{
    int32_t i;
    uint16_t intValue = 0;
    for (i = 1; i >= 0; i--)
        intValue = (intValue << 8) + byteValue[i];
    return intValue;
}

uint16_t UnsignedValue2ByteToInt(uint8_t *byteValue)
{
    int32_t i;
    uint16_t intValue = 0;
    for (i = 0; i <= 1; i++)
        intValue = (intValue << 8) + byteValue[i];
    return intValue;
}

void ValueIntTo2Byte(uint8_t *byteAry, int32_t value)
{
    int32_t i;
    for (i = 0; i < 2; i++)
    {
        byteAry[i] = value & 0xFF;
        value = value >> 8;
    }
}

void UnsignedValueIntTo2Byte(uint8_t *byteAry, uint16_t value)
{
    int32_t i;
    for (i = 0; i < 2; i++)
    {
        byteAry[i] = value & 0xFF;
        value = value >> 8;
    }
}

void ValueIntTo4ByteLe(uint8_t *byteAry, int32_t value)
{
    int32_t i;
    for (i = 0; i < 4; i++)
    {
        byteAry[i] = value & 0xFF;
        value = value >> 8;
    }
}

uint32_t UnsignedValue4ByteToUIntLe(uint8_t *byteValue)
{
    int32_t i;
    uint32_t uintValue = 0;
    for (i = 3; i >= 0; i--)
        uintValue = (uintValue << 8) + byteValue[i];
    return uintValue;
}

int32_t CheckLeapYear(int32_t iYear)
{
    if ((iYear % 4 == 0 && iYear % 100 != 0) || (iYear % 400 == 0))
        return 1;

    return 0;
}

void Unix2Time(uint64_t InUnixTime, int32_t *iYear, int32_t *iMonth, int32_t *iDate, int32_t *iHour, int32_t *iMin, int32_t *iSec)
{
    int64_t iDaysofMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int64_t iDaysofMonth2[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int64_t iTempYear, iTempMonth;
    int64_t TotalDay;
    int64_t lTempDay;
    int64_t SurplusSec;
    int64_t lLeapDay;
    int64_t lTemp;

    TotalDay = InUnixTime / (24 * 60 * 60);
    SurplusSec = InUnixTime % (24 * 60 * 60);

    iTempYear = 0;
    lTempDay = 0;
    while (1) // Get Years
    {
        if (CheckLeapYear(iTempYear + 1970))
            lLeapDay = 366;
        else
            lLeapDay = 365;

        lTempDay += lLeapDay;

        if ((TotalDay - lTempDay) < 0)
        {
            lTempDay -= lLeapDay;
            break;
        }
        iTempYear++;
    }

    lTempDay = TotalDay - lTempDay; //Surplus days

    lTemp = 0;
    for (iTempMonth = 0; iTempMonth < 12; iTempMonth++)
    {
        if (CheckLeapYear(iTempYear + 1970))
            lLeapDay = iDaysofMonth2[iTempMonth];
        else
            lLeapDay = iDaysofMonth[iTempMonth];

        lTemp += lLeapDay;
        if (lTemp > lTempDay)
        {
            lTemp -= lLeapDay;
            break;
        }
    }

    *iYear = iTempYear + 1970;
    *iMonth = iTempMonth + 1;
    *iDate = lTempDay - lTemp + 1;
    *iHour = SurplusSec / (60 * 60);
    SurplusSec = SurplusSec % (60 * 60);
    *iMin = SurplusSec / 60;
    *iSec = SurplusSec % 60;

    //return 0;
}

uint32_t Time2Unix(int32_t iYear, int32_t iMonth, int32_t iDate, int iHour, int iMin, int iSec)
{
    int32_t iDaysofMonth[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
    int32_t iTemp;
    int32_t LeapYearCount;
    uint32_t lSec;

    iTemp = iYear - 1970;
    LeapYearCount = 0;

    LeapYearCount = ((iTemp + 2) / 4); //計算1970 ~ iYear 總共經過幾個閏年

    if ((iTemp + 2) % 4 == 0) //如果iYear 是閏年
    {
        if (iMonth <= 2) //iYear 月份尚未超過02/29,要減1天回來
        {
            LeapYearCount--;
        }
        else if ((iMonth == 2) && (iDate == 29)) //iYear 月份剛好02/29,要加1天
        {
            LeapYearCount++;
        }
    }

    lSec = LeapYearCount + iTemp * 365 + iDaysofMonth[iMonth - 1] + iDate - 1;
    lSec = lSec * 24 + iHour;
    lSec = lSec * 60 + iMin;
    lSec = lSec * 60 + iSec;

    return lSec;
}

int32_t DateCmp(Date dt1, Date dt2)
{
    int32_t date1[6];
    int32_t date2[6];
    int32_t i;
    int32_t ret = 0;
    date1[0] = dt1.Year;
    date1[1] = dt1.Month;
    date1[2] = dt1.Day;
    date1[3] = dt1.Hour;
    date1[4] = dt1.Minute;
    date1[5] = dt1.Second;

    date2[0] = dt2.Year;
    date2[1] = dt2.Month;
    date2[2] = dt2.Day;
    date2[3] = dt2.Hour;
    date2[4] = dt2.Minute;
    date2[5] = dt2.Second;
    for (i = 0; i < 6; i++)
    {
        if (date1[i] > date2[i])
        {
            ret = 1;
            break;
        }
        else if (date1[i] < date2[i])
        {
            ret = -1;
            break;
        }
    }
    return ret;
}

void DosDateToDate(uint8_t *dosDate, Date *dt)
{
    uint16_t sDosDate;
    sDosDate = UnsignedValue2ByteToInt(dosDate);
    dt->Year = ((sDosDate >> 9) & 0x7F) + 1980;
    dt->Month = (sDosDate >> 5) & 0x0F;
    dt->Day = sDosDate & 0x1F;
    dt->Hour = 23;
    dt->Minute = 59;
    dt->Second = 59;
}

int32_t PackHex(char *Src, uint8_t *Dest, int32_t Length)
{
    int32_t i;
    for (i = 0; i < Length; i += 2)
    {
        Dest[i / 2] = (((Src[i] - '0') > 9 ? (Src[i] - '7') : Src[i] - '0') * 16) + ((Src[i + 1] - '0') > 9 ? (Src[i + 1] - '7') : Src[i + 1] - '0');
    }
    return (Length / 2);
}
uint16_t dictionaryFixedLengthValue(const uint8_t *file, const uint8_t *key, uint8_t *value, uint32_t valueLength)
{
    uint8_t *keyPosition;

    keyPosition = strstr(file, key);
    if (NULL == keyPosition)
    {
        return 1;
    }

    strncpy(value, keyPosition + strlen(key), valueLength);

    return 0;
}

uint16_t dictionaryNotFixedLengthValue(const uint8_t *file, const uint8_t *key, uint8_t *value, uint32_t *valueLength)
{
    uint8_t *keyPosition;
    uint8_t *endPosition;
    uint32_t keyLength = strlen(key);

    keyPosition = strstr(file, key);
    if (NULL == keyPosition)
    {
        return 1;
    }

    // Seeking "\x0D\x0A" in string.
    endPosition = strstr((keyPosition + keyLength), "\x0D\x0A");

    //> figure out the need len
    if (NULL == endPosition)
        *valueLength = 0;
    else
        *valueLength = (uint32_t)(endPosition - (keyPosition + keyLength));

    strncpy(value, keyPosition + keyLength, *valueLength);

    return 0;
}
