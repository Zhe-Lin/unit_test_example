#ifndef _UTILITY_H
#define _UTILITY_H

#include <stdint.h>

typedef struct
{
    int32_t Year;   // 2012,2011....
    int32_t Month;  // 1~12
    int32_t Day;    // 1~31
    int32_t Hour;   // 0~23
    int32_t Minute; //0~59
    int32_t Second; //0~59
} Date;

//--- Data Definition---

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

extern void GetRandom(uint8_t *);
extern int32_t Value4ByteComp_le(uint8_t *, uint8_t *);
extern int32_t Value4ByteToInt(uint8_t *);
extern uint16_t UnsignedValue2ByteToIntLe(uint8_t *);
extern uint16_t UnsignedValue2ByteToInt(uint8_t *byteValue);
extern void ValueIntTo2Byte(uint8_t *, int32_t);
extern void UnsignedValueIntTo2Byte(uint8_t *, uint16_t);
extern void ValueIntTo4ByteLe(uint8_t *byteAry, int32_t value);
extern uint32_t UnsignedValue4ByteToUIntLe(uint8_t *byteValue);
extern void Unix2Time(uint64_t InUnixTime, int32_t *iYear, int32_t *iMonth, int32_t *iDate, int32_t *iHour, int32_t *iMin, int32_t *iSec);
extern uint32_t Time2Unix(int32_t iYear, int32_t iMonth, int32_t iDate, int32_t iHour, int32_t iMin, int32_t iSec);
extern int32_t DateCmp(Date dt1, Date dt2);
extern void DosDateToDate(uint8_t *dosDate, Date *dt);
extern int32_t PackHex(char *Src, uint8_t *Dest, int32_t Length);
extern int32_t CheckLeapYear(int32_t iYear);
uint16_t dictionaryFixedLengthValue(const uint8_t *file, const uint8_t *key, uint8_t *value, uint32_t valueLength);
uint16_t dictionaryNotFixedLengthValue(const uint8_t *file, const uint8_t *key, uint8_t *value, uint32_t *valueLength);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _UTILITY_H
