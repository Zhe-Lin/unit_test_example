#ifndef _MATH_H
#define _MATH_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

int numberDouble(int number);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _MATH_H