#ifndef _DATA_PROCESS_H
#define _DATA_PROCESS_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

int decrease(void);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _DATA_PROCESS_H