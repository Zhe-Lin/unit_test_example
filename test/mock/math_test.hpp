#ifndef _MATH_TEST_H
#define _MATH_TEST_H

#include "test_framework_define.hpp"

#if (TEST_FRAMEWORK == DOCTEST)
#include <doctest/doctest.h>
#include <doctest/trompeloeil.hpp>

#elif (TEST_FRAMEWORK == CATCH2)
#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#endif

#include "common/math.h"
using trompeloeil::_;

class STUB_MATH
{
  public:
    MAKE_MOCK1(numberDouble, int(int));
};

extern STUB_MATH stubMath;

#endif // _MATH_TEST_H