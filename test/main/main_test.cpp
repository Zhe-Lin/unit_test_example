/**
 ******************************************************************************
 * File Name          : main_test.cpp
 * Description        : Main program body
 ******************************************************************************
 *
 *
 ******************************************************************************
 */
// Includes ------------------------------------------------------------------
#include "test_framework_define.hpp"

#if (TEST_FRAMEWORK == DOCTEST)
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>

#elif (TEST_FRAMEWORK == CATCH2)
#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch_all.hpp>
#endif

// Internal variables ---------------------------------------------------------

// Extern variables -----------------------------------------------------------

// Function--------------------------------------------------------------------
#if (TEST_FRAMEWORK == CATCH2)
int main(int argc, char *argv[])
{
    // global setup...

    int result = Catch::Session().run(argc, argv);

    // global clean-up...

    return result;
}
#endif
