cmake_minimum_required(VERSION 3.17)
project(project)

enable_language(ASM)

set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build." FORCE)

set(CMAKE_C_FLAGS_DEBUG "-O2 -Wno-register --coverage")
set(CMAKE_C_FLAGS_RELEASE "-O2")
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-O2 --coverage" )

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS_DEBUG "-O2 -Wno-register --coverage")
set(CMAKE_CXX_FLAGS_RELEASE "-O2")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 --coverage")

# add_definitions(-DSTM32F103xE)

# LINK_directories("/usr/local/lib")

include_directories(
		source
		test
		)

set(TEST_FRAMEWORK "Catch2" CACHE STRING "Choose test framework." FORCE)

if(TEST_FRAMEWORK STREQUAL "doctest")
    add_subdirectory(third_party/doctest)
    # message("select doctest")
endif()

if(TEST_FRAMEWORK STREQUAL "Catch2")
    # add_subdirectory(third_party/Catch2)
    # message("select Catch2")
endif()

add_subdirectory(third_party/trompeloeil)
add_subdirectory(source)
add_subdirectory(test)
